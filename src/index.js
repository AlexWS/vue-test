// JS
import './js/'

// SCSS
import './assets/scss/main.scss'

// CSS (example)
// import './assets/css/main.css'

// Vue.js
window.Vue = require('vue')

// Vue components (for use in html)
Vue.component('users-list', require('./js/components/Users.vue').default);
Vue.component('register', require('./js/components/Register.vue').default);
Vue.component('login', require('./js/components/Login.vue').default);
Vue.component('blog-posts', require('./js/components/Blog.vue').default);
// Vue init
const app = new Vue({
  el: '#app'
})
